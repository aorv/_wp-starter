<?php
/**
 * Template Name: Demo Page
 */
get_header();

// Gets featured image for page
$featured_img_id = get_post_thumbnail_id();
$featured_img_array = wp_get_attachment_image_src($featured_img_id, 'thumbnail-size', true);

if ( has_post_thumbnail() ) {
  $featured_img = '<img class="hero-img" src="' . $featured_img_array[0] . '" alt="">';
} else {
  $featured_img = '';
}
?>

<div class="wrapper" data-animate="on-load">
  <h1><?php the_title(); ?></h1>
  <?php echo $featured_img; ?>
  <!-- Demo Image -->
  <img width="200" src="<?php echo get_template_directory_uri(); ?>/assets/img/demo.jpg" alt="">
  <div class="content">
    <?php
      while ( have_posts() ) :
        the_post();
        the_content();
      endwhile;
		?>
  </div>
</div>

<?php
// get_sidebar();
get_footer();
