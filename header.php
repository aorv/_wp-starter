<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fountain
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#ffffff" />
	<meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="<?php echo esc_url( home_url( '/' ) ); ?>">
  <meta name="twitter:title" content="<?php bloginfo( 'name' ); ?>">
  <meta name="twitter:description" content="<?php bloginfo( 'description' ); ?>">
  <meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/meta-card-twitter.png">
  <meta property="og:title" content="<?php bloginfo( 'name' ); ?>">
  <meta property="og:url" content="<?php echo esc_url( home_url( '/' ) ); ?>">
  <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/img/meta-card.png">
  <meta property="og:description" content="<?php bloginfo( 'description' ); ?>">
  <meta property="og:site_name" content="<?php bloginfo( 'name' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300&display=swap" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-state="scroll">
	<header id="masthead" class="site-header">
		<div class="wrapper">

			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img class="logo__normal" width="200" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.svg" alt="<?php bloginfo( 'name' ); ?>">
			</a>

			<nav id="site-navigation" class="site-nav js-site-nav" data-state="closed">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
				?>
			</nav>

				<span class="mobile-menu-trigger js-menu-trigger">
					<ion-icon name="ios-menu" class="menu-trigger__icon js-menu-trigger-icon open" data-state="visible"></ion-icon>
					<ion-icon name="ios-close" class="menu-trigger__icon menu-trigger__icon--close js-menu-trigger-icon close" data-state="hidden"></ion-icon>
				</span>
		</div>
	</header>

	<div class="site-content">
