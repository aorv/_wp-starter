<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package fountain
 */

$featured_img_id = get_post_thumbnail_id();
$featured_img_array = wp_get_attachment_image_src($featured_img_id, 'thumbnail-size', true);

if ( has_post_thumbnail() ) {
  $featured_img = '<img class="hero-img" src="' . $featured_img_array[0] . '" alt="">';
} else {
  $featured_img = '';
}
?>

<div class="wrapper" data-animate="on-load">
  <h1><?php the_title(); ?></h1>
  <?php echo $featured_img; ?>
  <div class="content">
    <?php the_content();?>
  </div>
</div>
