<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package fountain
 */

$thumb_id = get_post_thumbnail_id();
$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
$thumb_url = $thumb_url_array[0];
?>


<header class="page__header" data-animate="on-load">
	<div class="col1-2">
		<?php the_title( '<h1 class="page__title">', '</h1>' ); ?>
	</div>
	<div class="page__intro col1-2">
		<?php the_content();?>
    <?php

      if(get_field('hidden_content'))
      {
        echo '<div class="collapsible js-collapsible" data-state="collapsed"><div class="collapsible__content js-collapsible-content">';
        echo get_field('hidden_content');
        echo '</div></div><span class="collapsible__trigger js-collapsible-trigger">More</span>';
      }

    ?>
	</div>
</header><!-- .entry-header -->

<div class="row" data-animate="on-scroll">
	<img data-hidden="mobile" class="aligncenter" src="<?php echo $thumb_url; ?>">
</div>

<section class="reduced">
  <div class="row displayflex" data-animate="on-scroll">
    <div class="col1-2">
      <?php

        if(get_field('service_img'))
        {
          echo '<img src="' . get_field('service_img') . '" alt="">';
        }

      ?>
    </div>
    <div class="col1-2">
    	<h2>Services Covered</h2>
      <?php

        if(get_field('service_content'))
        {
          echo get_field('service_content');
        }

      ?>
      <a class="button" href="<?php echo esc_url( home_url( '/' ) ); ?>contact">Get a Quote</a>
    </div>
  </div>
</section>

<?php get_template_part( 'template-parts/case', 'study' ); ?>