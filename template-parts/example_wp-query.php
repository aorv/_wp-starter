<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package fountain
 */

?>

<div class="services">
  <h2>Services</h2>
  <div class="slider-holder">
    <div class="slider js-slider">

    <?php
      $args = array(
          'post_type'      => 'page',
          'posts_per_page' => -1,
          'post_parent'    => 17,
          'orderby'        => 'menu_order',
          'order'          => 'ASC',
      );
      $parent = new WP_Query( $args );

      if ( $parent->have_posts() ) : ?>
        <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
          <div class="service">
            <img class="service__icon" width="100" src="<?php the_field('carousel_icon'); ?>" alt="">
            <h3 class="service__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <p class="service__desc"><?php the_field('carousel_text'); ?></p>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="slider__controls">
    <ion-icon class="slider__controls-item js-prev" name="ios-arrow-back"></ion-icon>
    <ion-icon class="slider__controls-item js-next" name="ios-arrow-forward"></ion-icon>
  </div>
</div>
