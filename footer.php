<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fountain
 */

?>

	</div><?php /* END .site-content */ ?>

	<footer id="colophon" class="site-footer">
		<div class="wrapper">
			<span class="copyright">&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>.
		</div>
	</footer>

	<?php wp_footer(); ?>
	<?php get_template_part( 'template-parts/analytics' ); ?>

	</body>
</html>
