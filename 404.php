<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fountain
 */

get_header();
?>

<div class="wrapper" data-animate="on-load">
  <h1>Error 404</h1>
  <p>Oops! That page doesn't exist, please return to the homepage or use the site menu.</p>
</div>

<?php get_template_part( 'template-parts/services' ); ?>

<?php
get_footer();
