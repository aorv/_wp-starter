const gulp         = require('gulp'),
      notify       = require('gulp-notify'),
      plumber      = require('gulp-plumber'),
      sass         = require('postcss-node-sass'),
      postcss      = require('gulp-postcss'),
      tailwindcss  = require('tailwindcss'),
      autoprefixer = require('autoprefixer'),
      cssnano      = require('cssnano'),
      uglify       = require('gulp-uglify-es').default,
      browserify   = require('browserify'),
      babelify     = require('babelify'),
      source       = require('vinyl-source-stream'),
      buffer       = require('vinyl-buffer'),
      hash         = require('gulp-hash'),
      clean        = require('gulp-clean');

const error = {
  handler: function(err) {
    notify.onError({
        title:    "Gulp",
        subtitle: "Failure!",
        message:  "Error: <%= error.message %>",
        sound:    "Beep"
    })(err);

    this.emit('end');
  }
}

const hashOptions = {
    hashLength: 4,
    template: '<%= name %>.min.<%= hash %>.<%= ext %>'
};

gulp.task('set-prod-node-env', function () {
  return process.env.NODE_ENV = 'production';
});

gulp.task('styles', [ 'clean:styles' ], function () {

  const plugins = [
    sass(),
    tailwindcss(),
    autoprefixer(),
    cssnano()
  ];

  return gulp.src('./scss/main.css')
    .pipe(plumber({errorHandler: error.handler}))
    .pipe(postcss(plugins))
    .pipe(hash(hashOptions))
    .pipe(gulp.dest('./assets/css/'))
    .on('end', function () {
      console.log('✅  CSS bundled successfully')
    })
})

gulp.task('scripts', [ 'clean:scripts' ], function () {
  return browserify({ debug: true })
    .transform(babelify)
    .require('./js/src.js', { entry: true })
    .bundle()
    .on('error', function (error) {
      notify().write(error)
      this.emit('end')
    })
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(hash(hashOptions))
    .pipe(gulp.dest('./assets/js/'))
    .on('end', function () {
      console.log('✅  JS bundled successfully')
    })
})

gulp.task('clean:styles', function () {
  return gulp.src('./assets/css/*', {read: false})
    .pipe(clean());
});

gulp.task('clean:scripts', function () {
  return gulp.src('./assets/js/*', {read: false})
    .pipe(clean());
});

gulp.task('default', [ 'set-prod-node-env', 'styles', 'scripts' ], function() {
  console.log('👀  Watching…')
  gulp.watch('./scss/**/*.scss', [ 'styles' ])
  gulp.watch('./**/*.php', ['styles'])
  gulp.watch('./js/**/*.js', [ 'scripts' ])
})
