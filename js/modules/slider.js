import Siema from 'siema'

const config = {
  el: '.js-slider',
  control: '.js-slider-pagination'
}

const servicesConfig = {
  selector: config.el,
  duration: 200,
  easing: 'ease-out',
  perPage: {
    0: 1,
    800: 2, // 2 items for viewport wider than 800px
    1100: 3 // 3 items for viewport wider than 1240px
  },
  startIndex: 0,
  draggable: true,
  multipleDrag: true,
  threshold: 20,
  loop: false,
  rtl: false
  // onChange: function () {
  //   $(`${config.control}[data-index="${this.currentSlide}"]`).attr('data-state', 'active').siblings().attr('data-state', '')
  // }
}

const testimonialsConfig = {
  selector: '.js-testimonials',
  duration: 200,
  easing: 'ease-out',
  perPage: {
    0: 1,
    800: 2, // 2 items for viewport wider than 800px
    1100: 3 // 3 items for viewport wider than 1240px
  },
  startIndex: 0,
  draggable: true,
  multipleDrag: true,
  threshold: 20,
  loop: false,
  rtl: false
}

const clientsConfig = {
  selector: '.js-clients',
  duration: 200,
  easing: 'ease-out',
  perPage: {
    0: 2, // 2 items for viewport wider than 800px
    1100: 3 // 3 items for viewport wider than 1240px
  },
  startIndex: 0,
  draggable: true,
  multipleDrag: true,
  threshold: 20,
  loop: false,
  rtl: false
}

// Siema.prototype.addPagination = function() {
//   for (let i = 0; i < this.innerElements.length; i++) {
//     const btn = document.createElement('span');
//     btn.setAttribute('data-index', i);
//     btn.setAttribute('class', 'slider__control js-slider-control');
//     btn.addEventListener('click', () => this.goTo(i));
//     this.selector.appendChild(btn);
//   }
// }


export class Slider {

  static showControls (slider) {
    document.querySelector('.js-prev').addEventListener('click', () => slider.prev());
    document.querySelector('.js-next').addEventListener('click', () => slider.next())
  }

  static getContentSlider () {
    const serviceSlider = new Siema(servicesConfig);
    Slider.showControls(serviceSlider);

    return serviceSlider
  }

  static init () {
    const testimonialsSlider = new Siema(testimonialsConfig);
    const clientsSlider = new Siema(clientsConfig);
    // slider.addPagination()
    // $(`${config.control}[data-index="0"]`).attr('data-state', 'active')
  }
}
