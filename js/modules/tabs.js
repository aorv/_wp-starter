import $ from 'jquery'

const config = {
  trigger: '.js-tab-item',
  tabTarget: '.js-tab-target',
  activeClass: 'is-active'
}

export class Tabs {

  static toggle () {
    $(config.trigger).on('click', function () {
      const currentTab = $(this);
      const target = currentTab.attr('data-target');

      $(config.trigger).removeClass(config.activeClass);
      currentTab.toggleClass(config.activeClass);

      $(config.tabTarget).attr('data-state', 'hidden');
      $(target).attr('data-state', 'visible');
    });
  }

  static init () {
    $(config.tabTarget).not(':eq(0)').attr('data-state', 'hidden');
    Tabs.toggle()
  }
}
