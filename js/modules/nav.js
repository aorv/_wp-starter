import $ from 'jquery'

const config = {
  trigger: '.js-menu-trigger',
  triggerIcon: '.js-menu-trigger-icon',
  dropdownParent: '.menu-item-has-children',
  dropdownMenu: '.sub-menu',
  breakpoint: 800,
  el: '.js-site-nav'
}

const subMenu = `${config.dropdownParent} > ${config.dropdownMenu}`

export class Nav {
  static disableSiteScroll () {
    $('body').attr('data-state', ($('body').attr('data-state') == 'scroll' ? 'no-scroll' : 'scroll'));
  }

  static toggle (el) {
    $(el).attr('data-state', ($(el).attr('data-state') == 'open' ? 'closed' : 'open'));
  }

  static iconToggle (el) {
    $(el).each(function () {
      $(this).attr('data-state', ($(this).attr('data-state') == 'visible' ? 'hidden' : 'visible'))
    })
  }

  static closeMobileNav () {
    $(config.el).attr('data-state', 'closed');
    $(`${config.triggerIcon}.open`).attr('data-state', 'visible');
    $(`${config.triggerIcon}.close`).attr('data-state', 'hidden');
  }

  static closeDropdown () {
    $('.js-sub-menu-arrow').attr('name', 'ios-arrow-down')
    $(subMenu).attr('data-state', 'hidden')
  }

  static dropdownMenu () {
    const downArrow =   '<ion-icon class="sub-menu-arrow js-sub-menu-arrow" name="ios-arrow-down"></ion-icon>'

    $(`${config.dropdownParent} > a`).attr('data-no-swup', '').append(downArrow).on('click', function (e) {

      $('.js-sub-menu-arrow').attr('name', ($('.js-sub-menu-arrow').attr('name') == 'ios-arrow-down' ? 'ios-arrow-up' : 'ios-arrow-down'))

      if (window.innerWidth > config.breakpoint) {
        e.preventDefault();
        $(subMenu).attr('data-state', ($(subMenu).attr('data-state') == 'visible' ? 'hidden' : 'visible'))
      }
    });
  }

  static init () {
    Nav.dropdownMenu()

    $(config.trigger).on('click', function () {
      Nav.toggle(config.el)
      Nav.iconToggle(config.triggerIcon)

      if (window.innerWidth <= config.breakpointTablet) {
        Nav.disableSiteScroll()
      }
    });
  }
}
