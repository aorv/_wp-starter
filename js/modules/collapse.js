import $ from 'jquery'

const config = {
  trigger: '.js-collapsible-trigger',
  arrow: 'js-collapsible-arrow',
  el: '.js-collapsible-content',
  breakpoint: 1100
}

export class Collapse {

  static toggleCollapse () {
    const downArrow =   `<ion-icon class="${config.arrow} collapsible__arrow" name="ios-arrow-down"></ion-icon>`

    $(config.trigger).append(downArrow).on('click', function () {
      const targetEL = $(this).siblings('.js-collapsible').children(config.el)
      const arrow    = $(this).children(`.${config.arrow}`)

      arrow.attr('name', (arrow.attr('name') == 'ios-arrow-down' ? 'ios-arrow-up' : 'ios-arrow-down'));
      targetEL.attr('data-state', (targetEL.attr('data-state') == 'collapsed' ? 'open' : 'collapsed')).slideToggle();
    });
  }

  static init () {
    Collapse.toggleCollapse()
  }
}
