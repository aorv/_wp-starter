import $ from 'jquery'

const config = {
  elScroll: '[data-animate="on-scroll"]',
  elLoad: '[data-animate="on-load"]',
  state: 'animate',
  offset: window.innerHeight * 0.7,
  delay: 50
}

export class ScrollAnimate {
  static getViewportTop () {
    return $(window).scrollTop();
  }

  static elDistance (el) {
    return $(el).offset().top - config.offset;
  }

  static init () {
    $(config.elLoad).attr('data-state', config.state)

    $(window).on( 'scroll', function () {
      const viewportTop = ScrollAnimate.getViewportTop()

      $(config.elScroll).each(function (i) {
        const el = $(this)
        if (viewportTop >= ScrollAnimate.elDistance(el)) {

          if (el.data('animateDelay')) {
            setTimeout(function () {
              el.attr('data-state', config.state)
            }, config.delay * i)
          } else {
            el.attr('data-state', config.state)
          }
        }
      })
    })
  }
}