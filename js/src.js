import $ from 'jquery'
// import Rellax from 'rellax'
// import Swup from 'swup'
// import SwupSlideTheme from '@swup/slide-theme'
// import SwupScrollPlugin from '@swup/scroll-plugin';
// import SwupBodyClassPlugin from '@swup/body-class-plugin';
// import SwupGaPlugin from '@swup/ga-plugin';
// import { Nav } from './modules/nav'
// import { Slider } from './modules/slider'
// import { Tabs } from './modules/tabs'
// import { ScrollAnimate } from './modules/scroll-animate'
// import { Collapse } from './modules/collapse'


// if ($('body').hasClass('home')) {
//   const rellax = new Rellax('[data-action="animate"]');
// }

const body = $('body');

function init() {
  // Slider.getContentSlider();
  if (!body.hasClass('blog') && !body.hasClass('single')) {
    Slider.getContentSlider().destroy(true, Slider.getContentSlider);
  }
}

init();
Slider.init();
Nav.init();
Tabs.init();

const swupOptions = {
  animateHistoryBrowsing: true,
  cache: false,
  plugins: [
    new SwupSlideTheme(),
    new SwupScrollPlugin({ animateScroll: false }),
    new SwupBodyClassPlugin(),
    new SwupGaPlugin()
  ]
}

if (!body.hasClass('admin-bar')) {
  const swup = new Swup(swupOptions);

  swup.on('clickLink', function (e) {
    // $('.menu-item a').removeClass('active')
    // $(e.path[0]).addClass('active')
    Nav.closeDropdown();
    Nav.closeMobileNav();
  })

  swup.on('contentReplaced', init);
  // swup.on('willReplaceContent', unload);
}



// ScrollAnimate.init()
// Collapse.init()
